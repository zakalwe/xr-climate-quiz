import Question from "@/types/question";

interface QuestionItem {
  question: Question;
  ordinal: number;
  total: number;
}

export default QuestionItem;
