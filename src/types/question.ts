interface Question {
  prompt: string;
  choices: Array<string>;
  answer: number;
  sourceLabel?: string;
  sourceLink?: string;
  chosen: number;
}

export default Question;
