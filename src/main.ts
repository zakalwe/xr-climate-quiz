import Vue from "vue";
import App from "./App.vue";
import VueSocialSharing from "vue-social-sharing";

Vue.use(VueSocialSharing);

Vue.config.productionTip = false;

import "@/assets/css/quiz.css"

new Vue({
  render: h => h(App)
}).$mount("#app");
