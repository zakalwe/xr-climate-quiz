module.exports = {
    chainWebpack: config => {
      config.plugin('html').tap(args => {
        if (args[0].minify) {
          args[0].minify.removeAttributeQuotes = false;
        }
        return args;
      })
    },
    css: {
      loaderOptions: {
        sass: {
          additionalData: `
            @import "@/assets/css/_variables.scss";
          `
        }
      }
    }
  };